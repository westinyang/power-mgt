# PowerMGT

<img src="entry/src/main/resources/base/media/icon.png" width="128px" />

## 项目介绍

电源管理，特别为小米6、一加6T、PocoF1等OH旧手机准备的电源键拯救者

- 现阶段OpenHarmony系统关机重启需要按下电源键大概4秒左右才弹出，这对于本身就“年事已高”、刷入OH的旧手机来说是很致命的，说不定哪天电源键坏掉了，进个BL都麻烦
- 所以用软件控制关机重启，开机的话本身就可以插电自启，这样就可以解放电源键啦
- 同时还提供了电源模式切换功能

## 开发环境

> [Full-SDK编译和替换指南](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Release/zh-cn/application-dev/quick-start/full-sdk-compile-guide.md)

- DevEco Studio 3.1 Release
- SDK API9 3.2.12.5 Release (Full-SDK)

## 截图预览

<img src="screenshot/01.jpg" width="300px" />&emsp;<img src="screenshot/02.jpg" width="300px" />

## 视频演示

https://www.bilibili.com/video/BV1qs4y1A7fU

## 许可声明

- 应用图标出处：https://www.iconarchive.com/show/papirus-apps-icons-by-papirus-team/preferences-system-power-icon.html
- 其他图标出处：https://www.flaticon.com/free-icon/info_9723316